# Deployment
For deploying the test environment

## Requirements
- pip3 install hcloud
- ansible-galaxy role install -r requirements.yml
- ansible-galaxy collection install -r requirements.yml


## Ansible

###
Ensure the traefik role is deployed first on first run, package deps will there also be solved

`source test.env; HCLOUD_TOKEN=xxx PASSWORD_STORE_DIR="Where you clone you flipdot_passwords repo (see https://gitlab.com/flipdot/hosting/passwordstore/-/blob/master/README.MD)" ansible-playbook -e env=test -i inventory/hcloud.yml deploy.yml`

## docker-compose
`source test.env; TELEGRAM_BOT_TOKEN=xxx TELEGRAM_CHANNEL=xxx docker-compose up`
