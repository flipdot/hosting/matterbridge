[irc]
  [irc.freenode]
    Server="irc.freenode.net:6667"
    Nick="${IRC_NICK}"
    RemoteNickFormat="<{NICK}> "
    ColorNicks=true
    Charset="utf-8"

[telegram]
  [telegram.secure]
    Token="${TELEGRAM_BOT_TOKEN}"
    RemoteNickFormat="<{NICK}> "


[[gateway]]
name="gateway1"
enable=true

  [[gateway.inout]]
    account="irc.freenode"
    channel="${IRC_CHANNEL}"
  [[gateway.inout]]
    account="telegram.secure"
    channel="${TELEGRAM_CHANNEL}"

[general]
MediaDownloadPath="/var/www/matterbridge"
MediaServerDownload="https://bridge.${BASE_DOMAIN}"
