#!/bin/sh

apk add gettext
envsubst < "/etc/matterbridge/matterbridge.toml.tpl" > "/etc/matterbridge/matterbridge.toml"

exec "$@"
